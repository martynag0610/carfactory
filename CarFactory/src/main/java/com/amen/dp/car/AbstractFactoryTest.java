package com.amen.dp.car;

import org.junit.Test;

import org.junit.*;

public class AbstractFactoryTest {

	@Test
	public void test1(){
		ICar car = CarFactory.createBMW3();
		
		Assert.assertEquals(250.0, car.getMaxSpeed(), 0);
		Assert.assertEquals(0.8, car.getAccel(), 0);
	}
}
