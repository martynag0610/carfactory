package com.amen.dp.car;

class AbstractCar implements ICar {

	private String brand;
	private double maxSpeed;
	private double accel;
	
	
	
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public double getMsxSpeed() {
		return maxSpeed;
	}

	public void setMsxSpeed(double msxSpeed) {
		this.maxSpeed = msxSpeed;
	}

	public double getAccel() {
		return accel;
	}

	public void setAccel(double accel) {
		this.accel = accel;
	}

	
	public AbstractCar(String brand, double maxSpeed, double accel) {
		this.brand = brand;
		this.maxSpeed = maxSpeed;
		this.accel = accel;
	}



	public void drive() {
	System.out.println("Samochod"  + brand + " o max " + maxSpeed + "i przyspiesza" + accel);
		
	}

	@Override
	public double getMaxSpeed() {
		return maxSpeed;
	}
}
