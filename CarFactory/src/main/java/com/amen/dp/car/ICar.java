package com.amen.dp.car;

public interface ICar {
	void drive();
	double getAccel();
	double getMaxSpeed();
}
