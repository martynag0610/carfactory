package com.amen.dpd;

import com.amen.dp.car.CarFactory;
import com.amen.dp.car.ICar;

public class Main {

	public static void main(String[] args) {
	 
		ICar car = CarFactory.createBMW3();
		ICar carCivic = CarFactory.createHondaCivic();
		
		car.drive();
		carCivic.drive();
	}

}
