package sda.nordea.compozyt;


import java.util.LinkedList;
import java.util.List;

public class Manager implements Worker {
	private String name;
	private List<Worker> workers = new LinkedList<>();
	
	
	
	public Manager(String name) {
		super();
		this.name = name;
	}

	@Override
	public void introduce() {
		System.out.println("Im " + name + " im an empolyess : { " );

			for (Worker worker : workers){
				worker.introduce();
			}
	}
	
	public void add(Worker worker){
		workers.add(worker);
	}
	
	
}




