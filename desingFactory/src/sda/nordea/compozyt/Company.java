package sda.nordea.compozyt;

public class Company {

	
	public static void main(String[] args) {
		
		Manager president = new Manager("Andrzej");
		Worker w = new SimpleWorker("Jan","Programista");
		Worker w2 = new SimpleWorker("Janina", "Administrator");
		Manager m = new Manager("Jozefik");
		Worker w3 = new SimpleWorker("Adam", "malysz");
		
		
		president.add(w);
		president.add(w2);
		president.add(m);
		m.add(w3);
		
		president.introduce();
	}
}
