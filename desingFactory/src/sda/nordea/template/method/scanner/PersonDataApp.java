package sda.nordea.template.method.scanner;

import java.util.Scanner;

public class PersonDataApp extends StandardConsoleAppTempleteMethod{

	private String name;
	private String surname;
	
	
	@Override
	public void showResult() {
	
		System.out.println("Imie: " + name);
		System.out.println("Nazwisko: " + surname);

	}

	@Override
	public void calulateResult() {}

	@Override
	public void getData(Scanner scanner) {
		System.out.println("Podaj imie: ");
		name = scanner.next();
		System.out.println("Podaj nazwisko: ");
		surname = scanner.next();
	}
	
	public void main(String[] args) {
		new PersonDataApp().main();
		
	}
}
