package sda.nordea.template.method.scanner;

import java.util.InputMismatchException;
import java.util.Scanner;

public abstract class StandardConsoleAppTempleteMethod {
	
	public void main() {
		
		
		Scanner scanner = new Scanner(System.in);
		boolean validData = false;
		do{
			try{
				getData(scanner);
				validData = true;
			}catch (InputMismatchException exception){		
				System.out.println("podaj dane jeszcze raz");
				scanner.reset();
				scanner.next();
			}
		}while (!validData);
		calulateResult();
		showResult();
		
	}

	public abstract void showResult();
	public abstract void calulateResult();
	public abstract void getData(Scanner scanner);

}
