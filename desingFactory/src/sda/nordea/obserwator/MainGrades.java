package sda.nordea.obserwator;

public class MainGrades {

	public static void main(String[] args) {
 Grade grades = new Grade();
 
 GradesOberver randomObserver = new GradesOberver() {
	
	@Override
	public void newGrade(int grade) {
		System.out.println("0, jakas ocena " + grade);
		
	}
};
  
grades.addObserver(randomObserver);


grades.addGrade(1);
grades.addGrade(2);
grades.addGrade(3);
grades.addGrade(4);
grades.addGrade(5);

	}

}
