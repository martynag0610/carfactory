package sda.nordea.obserwator;

import java.util.LinkedList;
import java.util.List;

public class Grade {

	private List<Integer> gradelist = new LinkedList<>();
	private List<GradesOberver> observers = new LinkedList<>();
	
	public void addGrade(int grade){
		gradelist.add(grade);
		
		for (GradesOberver gradesOberver : observers) {
			gradesOberver.newGrade(grade);
		}
	}
	
	
	public void addObserver(GradesOberver greadesObserver){
		observers.add(greadesObserver);
	}
}
