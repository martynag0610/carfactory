package sda.noredea.users;
import java.security.Permissions;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

public class User {

	private String nickname;
	private String password;
	private UserType type;

	public User(String nickname, String password, UserType type) {
		super();
		this.nickname = nickname;
		this.password = password;
		this.type.kind = kind;
		this.type.permissions = EnumSet.copyOf(permissions);
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getKind() {
		return type.kind;
	}

	public void setKind(String kind) {
		this.type.kind = kind;
	}

	public EnumSet<Permission> getPermission() {
		return type.permissions;
	}

	public void setPermission(Collection<Permission> permissions) {
		this.type.permissions = EnumSet.copyOf(permissions);
	}


}
