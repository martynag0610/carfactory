package sda.noredea.users;
import java.util.Arrays;   //trzeba zaimpotowac tego enuma ale nie mma nazwy pakietu wiec dupa
import static sda.noredea.users.Permission.*;
public class Main {

	public static void main(String[] args) {

		UserFactory factory = new UserFactory();

		factory.addPrototype(new User("", "", "klient", Arrays.asList(CREATE_ORDERS)));
		factory.addPrototype(new User("", "", "manager", Arrays.asList(Permission.values())));
		factory.addPrototype(new User("", "", "sprzedawca", Arrays.asList(
				CREATE_PRODUCTS,
				DELETE_PRODUCTS,
				)));

		User user = factory.create("edek", "23", "klient");
	}
}