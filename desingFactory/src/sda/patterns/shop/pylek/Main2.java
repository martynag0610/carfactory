package sda.patterns.shop.pylek;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main2 {
public static void main(String[] args) {
	

	Shop shop = new Shop();
	shop.addProduct(new Product("Maslo", 5, "maslane"));
	shop.addProduct(new Product("chleb", 2, "pe�noziarnisty"));
	shop.addProduct(new Product("Maka", 2, "pszenna"));
	
	
	Random r = new Random();
	List<Product> products = new ArrayList<>();
	
	String[] names = {"Maslo", "chleb", "Maka"};
	for (int i=0; i<10; i++){
		String name = names[r.nextInt(3)]; // losuje element tablicy
		Product product = shop.getProduct(name);  // pobieram product za sklepu
		products.add(product);
	}
	
	for (Product p : products) {
		System.out.println(p.getName() + " - " + p.getPrice() + " z� ");
	}
}
	
}
